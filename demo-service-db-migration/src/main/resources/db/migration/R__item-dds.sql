CREATE TABLE IF NOT EXISTS `dds` (
    `id` INT AUTO_INCREMENT,
    `customer_name` VARCHAR(30) NOT NULL,
    `phone`  VARCHAR(30) ,
    PRIMARY KEY (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

SET @add_key_type_to_report = (SELECT IF(
    (SELECT count(1)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'dds'
        AND table_schema = DATABASE()
        AND column_name = 'age'
    ) > 0,
    "select 1",
    "alter table dds add `age` int(4) NOT NULL"
));
PREPARE stmt FROM @add_key_type_to_report;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @add_key_type_to_report = (SELECT IF(
    (SELECT count(1)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'dds'
        AND table_schema = DATABASE()
        AND column_name = 'address'
    ) > 0,
    "select 1",
    "alter table dds add `address` varchar(30)"
));
PREPARE stmt FROM @add_key_type_to_report;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @add_key_type_to_report = (SELECT IF(
    (SELECT count(1)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'dds'
        AND table_schema = DATABASE()
        AND column_name = 'aa'
    ) > 0,
    "select 1",
    "alter table dds add `aa` varchar(30)"
));
PREPARE stmt FROM @add_key_type_to_report;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

