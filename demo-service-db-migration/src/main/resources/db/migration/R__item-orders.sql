CREATE TABLE IF NOT EXISTS `orders` (
    `id` VARCHAR(30) NOT NULL,
    `order_name` VARCHAR(30) NOT NULL,
    `price`  DOUBLE ,
    `address` VARCHAR(50) NOT NULL,
    `customer_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

SET @add_key_type_to_report = (SELECT IF(
    (SELECT count(1)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = 'orders'
        AND table_schema = DATABASE()
        AND column_name = 'updated_time'
    ) > 0,

    "select 1",
    "alter table orders add `updated_time` DATETIME NULL"
));
PREPARE stmt FROM @add_key_type_to_report;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;