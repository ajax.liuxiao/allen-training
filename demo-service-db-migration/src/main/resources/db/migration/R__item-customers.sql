CREATE TABLE IF NOT EXISTS `customers` (
    `id` INT AUTO_INCREMENT,
    `customer_name` VARCHAR(30) NOT NULL,
    `phone`  VARCHAR(30) ,
    PRIMARY KEY (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

# 1.use beautiful format , such as
# id               INT             AUTO_INCREMENT,
# customer_name    VARCHAR(30)     NOT NULL,
# phone            VARCHAR(30) ,
# 2.rename file name , for example "R_item_customers.sql"
