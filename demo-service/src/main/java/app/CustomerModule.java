package app;

import app.demo.api.BOCustomerWebService;
import app.demo.api.CustomerWebService;
import app.demo.api.customer.kafka.TestPublishMessage;
import app.demo.customer.domain.Bloginfo;
import app.demo.customer.domain.Customer;
import app.demo.customer.kafka.TestBulkMessageHandler;
import app.demo.customer.kafka.TestPublishMessageHandler;
import app.demo.customer.service.BOCustomerService;
import app.demo.customer.service.CustomerService;
import app.demo.customer.web.BOCustomerWebServiceImpl;
import app.demo.customer.web.CustomerWebServiceImpl;
import core.framework.module.Module;
import core.framework.mongo.module.MongoConfig;

/**
 * @author Allen
 */
public class CustomerModule extends Module {
    @Override
    protected void initialize() {
        executor().add();
        MongoConfig config = config(MongoConfig.class);
        config.uri(requiredProperty("sys.mongo.uri"));
        config.collection(Bloginfo.class);

        db().repository(Customer.class);

        kafka().uri(requiredProperty("app.kafka.uri"));
        kafka().publish("test-topic", TestPublishMessage.class);
        kafka().subscribe("test-topic", TestPublishMessage.class, bind(TestPublishMessageHandler.class)); //TODO  handler need start with message name, for example : TestPublishMessageHandler
        kafka().subscribe("test-bulk", TestPublishMessage.class, bind(TestBulkMessageHandler.class));

        bind(CustomerService.class);
        bind(BOCustomerService.class);
        api().service(CustomerWebService.class, bind(CustomerWebServiceImpl.class));
        api().service(BOCustomerWebService.class, bind(BOCustomerWebServiceImpl.class));
    }
}
