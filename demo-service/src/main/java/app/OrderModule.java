package app;

import app.demo.api.BOOrderWebService;
import app.demo.api.OrderWebService;
import app.demo.api.order.SearchOrderSaleView;
import app.demo.order.domain.Order;
import app.demo.order.service.BOOrderService;
import app.demo.order.service.OrderService;
import app.demo.order.web.BOOrderWebServiceImpl;
import app.demo.order.web.OrderWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class OrderModule extends Module {
    @Override
    protected void initialize() {
        //TODO remove useless blank line.
        db().repository(Order.class);
        db().view(SearchOrderSaleView.class);
        bind(OrderService.class);
        bind(BOOrderService.class);
        api().service(OrderWebService.class, bind(OrderWebServiceImpl.class));
        api().service(BOOrderWebService.class, bind(BOOrderWebServiceImpl.class));
    }
}
