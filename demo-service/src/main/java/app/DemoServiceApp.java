package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Allen
 */
public class DemoServiceApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8081);
        load(new SystemModule("sys.properties"));
        loadProperties("app.properties");
        load(new CustomerModule());
//        load(new JobModule());
        load(new OrderModule());
    }
}
