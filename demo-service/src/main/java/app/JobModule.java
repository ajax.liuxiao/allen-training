package app;

import app.demo.job.DemoJob;
import core.framework.module.Module;

import java.time.Duration;

/**
 * @author Allen
 */
public class JobModule extends Module {
    @Override
    protected void initialize() {
        DemoJob job = bind(DemoJob.class);
        schedule().fixedRate("fixed-rate-job", job, Duration.ofSeconds(10));
    }
}
