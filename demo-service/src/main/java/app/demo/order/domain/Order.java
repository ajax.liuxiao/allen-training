package app.demo.order.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;
import core.framework.mongo.Id;

import java.time.LocalDateTime;

/**
 * @author Allen
 */
@Table(name = "orders")
public class Order {
    @Id
    @PrimaryKey
    @Column(name = "id")
    public String id;

    @NotNull
    @Column(name = "order_name")
    public String orderName;

    @Column(name = "price")
    public String price;

    @NotNull
    @Column(name = "address")
    public String address;

    @NotNull
    @Column(name = "customer_id")
    public String customerId;

    @Column(name = "status")
    public String status;

    @Column(name = "updated_time")
    public LocalDateTime updatedTime;
}
