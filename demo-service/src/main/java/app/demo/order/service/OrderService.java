package app.demo.order.service;

import app.demo.api.order.OrderView;
import app.demo.api.order.SearchOrderRequest;
import app.demo.api.order.SearchOrderResponse;
import app.demo.api.order.UpdateOrderStatusRequest;
import app.demo.customer.domain.Customer;
import app.demo.order.domain.Order;
import core.framework.db.Database;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * @author Allen
 */
public class OrderService {
    @Inject
    Repository<Order> orderRepository;
    @Inject
    Repository<Customer> customerRepository;
    @Inject
    Database database;

    public SearchOrderResponse search(SearchOrderRequest request) {
        SearchOrderResponse response = new SearchOrderResponse();
        Query<Order> query = orderRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.id)) {
            query.where("id = ?", request.id);
        }
        if (!Strings.isBlank(request.orderName)) {
            query.where("order_name LIKE ?", Strings.format("{}%", request.orderName));
        }
        if (!Strings.isBlank(request.address)) {
            query.where("address LIKE ?", Strings.format("{}%", request.address)); // SQL keywords  use uppercase: LIKE
        }
        if (request.customerId != null) {
            query.where("customer_id = ?", request.customerId);
        }

        response.orders = new ArrayList<>();
        response.customers = new ArrayList<>();
        query.fetch().stream().forEach(order -> {
            Customer customer = customerRepository.get(order.customerId).orElseThrow(() -> new NotFoundException("customer not found id", Strings.format("{}", order.customerId)));
            SearchOrderResponse.Order order1 = new SearchOrderResponse.Order();
            SearchOrderResponse.Customer customer1 = new SearchOrderResponse.Customer();
            order1.id = order.id;
            order1.address = order.address;
            response.orders.add(order1);
            customer1.customerName = customer.customerName;
            customer1.phone = customer.phone;
            response.customers.add(customer1);
        });

        response.total = Long.valueOf(query.count());
        return response;
    }

    public OrderView updateStatus(UpdateOrderStatusRequest request) {
        Order order = orderRepository.get(request.id).orElseThrow(() -> new NotFoundException("order not found id", Strings.format("{}", request.id)));
        order.status = request.status;
        order.updatedTime = LocalDateTime.now();
        orderRepository.partialUpdate(order);
        return orderView(order);
    }

    private OrderView orderView(Order order) {
        OrderView orderView = new OrderView();
        orderView.id = order.id;
        orderView.orderName = order.orderName;
        orderView.address = order.address;
        orderView.price = order.price;
        orderView.customerId = order.customerId;
        orderView.status = order.status;
        orderView.updatedTime = order.updatedTime;
        return orderView;
    }
}
