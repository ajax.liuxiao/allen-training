package app.demo.order.service;

import app.demo.api.order.SearchOrderSaleRequest;
import app.demo.api.order.SearchOrderSaleResponse;
import app.demo.api.order.SearchOrderSaleView;
import app.demo.order.domain.Order;
import core.framework.db.Database;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Allen
 */
public class BOOrderService {
    private final Logger logger = LoggerFactory.getLogger(BOOrderService.class);
    @Inject
    Repository<Order> repository;
    @Inject
    Database database;
    public SearchOrderSaleResponse search(SearchOrderSaleRequest request) {
        try {
            SearchOrderSaleView searchOrderSaleView = database.selectOne("SELECT SUM(price) as total_money FROM orders WHERE DATE_FORMAT(CURDATE(),'%Y-%m') = DATE_FORMAT( ?, '%Y-%m') AND status = 3", SearchOrderSaleView.class, request.month).orElseThrow();
            return view(searchOrderSaleView);
        } catch (Exception e) {
            logger.error("SEARCH_ERROR", e);
            throw e;
        }
    }

    private SearchOrderSaleResponse view(SearchOrderSaleView saleView) {
        SearchOrderSaleResponse searchOrderSaleResponse = new SearchOrderSaleResponse();
        logger.info("-----------------------------------" + saleView.totalMoney);
        searchOrderSaleResponse.totalMoney = saleView.totalMoney;
        return searchOrderSaleResponse;
    }
}
