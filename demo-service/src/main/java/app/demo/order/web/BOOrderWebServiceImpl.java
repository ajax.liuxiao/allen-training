package app.demo.order.web;

import app.demo.api.BOOrderWebService;
import app.demo.api.order.SearchOrderSaleRequest;
import app.demo.api.order.SearchOrderSaleResponse;
import app.demo.order.service.BOOrderService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class BOOrderWebServiceImpl implements BOOrderWebService {
    @Inject
    BOOrderService boOrderService;
    @Override
    public SearchOrderSaleResponse search(SearchOrderSaleRequest request) {
        return boOrderService.search(request);
    }
}
