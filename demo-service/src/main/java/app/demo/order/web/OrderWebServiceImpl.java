package app.demo.order.web;

import app.demo.api.OrderWebService;
import app.demo.api.order.OrderView;
import app.demo.api.order.SearchOrderRequest;
import app.demo.api.order.SearchOrderResponse;
import app.demo.api.order.UpdateOrderStatusRequest;
import app.demo.order.service.OrderService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class OrderWebServiceImpl implements OrderWebService {
    @Inject
    OrderService orderService;

    @Override
    public SearchOrderResponse search(SearchOrderRequest request) {
        return orderService.search(request);
    }

    @Override
    public OrderView updateStatus(UpdateOrderStatusRequest request) {
        return orderService.updateStatus(request);
    }
}
