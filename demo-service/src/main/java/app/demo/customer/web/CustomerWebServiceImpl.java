package app.demo.customer.web;

import app.demo.api.CustomerWebService;
import app.demo.api.customer.CreateCustomerRequest;
import app.demo.api.customer.CustomerView;
import app.demo.api.customer.bloginfo.SearchBlogInfoRequest;
import app.demo.api.customer.bloginfo.SearchBlogInfoResponse;
import app.demo.customer.service.CustomerService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author Allen
 */
public class CustomerWebServiceImpl implements CustomerWebService {
    @Inject
    CustomerService customerService;

    @Override
    public CustomerView get(Long id) {
        return customerService.get(id);
    }

    @Override
    public CustomerView create(CreateCustomerRequest request) {
        ActionLogContext.put("customerName", request.customerName); //TODO correct key is customerName
        ActionLogContext.put("test", "-------------------------test----------------------------------");
        return customerService.create(request);
    }

    @Override
    public void publishTest() {
        customerService.publishTest();
    }

    @Override
    public SearchBlogInfoResponse search(SearchBlogInfoRequest request) {
        return customerService.search(request);
    }
}
