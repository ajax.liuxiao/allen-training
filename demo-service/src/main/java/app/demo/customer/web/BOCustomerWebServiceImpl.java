package app.demo.customer.web;

import app.demo.api.BOCustomerWebService;
import app.demo.api.customer.BOCustomerView;
import app.demo.api.customer.BOSearchCustomerRequest;
import app.demo.api.customer.BOSearchCustomerResponse;
import app.demo.customer.service.BOCustomerService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class BOCustomerWebServiceImpl implements BOCustomerWebService {
    @Inject
    BOCustomerService boCustomerService;

    @Override
    public BOSearchCustomerResponse search(BOSearchCustomerRequest request) {
        return boCustomerService.search(request);
    }

    @Override
    public BOCustomerView searchOne(BOSearchCustomerRequest request) {
        return boCustomerService.searchOne(request);
    }
}
