package app.demo.customer.service;

import app.demo.api.customer.CreateCustomerRequest;
import app.demo.api.customer.CustomerView;
import app.demo.api.customer.bloginfo.BlogInfoView;
import app.demo.api.customer.bloginfo.SearchBlogInfoRequest;
import app.demo.api.customer.bloginfo.SearchBlogInfoResponse;
import app.demo.api.customer.kafka.TestPublishMessage;
import app.demo.customer.domain.Bloginfo;
import app.demo.customer.domain.Customer;
import com.mongodb.client.model.Filters;
import core.framework.async.Executor;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class CustomerService {
    private final Logger logger = LoggerFactory.getLogger(CustomerService.class);
    //  TODO remove useless blank line
    @Inject
    Repository<Customer> customerRepository;
    @Inject
    MongoCollection<Bloginfo> blogInfoCollection;
    @Inject
    MessagePublisher<TestPublishMessage> publisher;
    @Inject
    Executor executor;

    public CustomerView get(Long id) {
        Customer customer = customerRepository.get(id).orElseThrow(() -> new NotFoundException("customer not found id", Strings.format("{}", id)));
        return view(customer);
    }

    public CustomerView create(CreateCustomerRequest request) {
        Customer customer = new Customer();
        customer.customerName = request.customerName;
        customer.phone = request.phone;
        customer.id = customerRepository.insert(customer).orElseThrow();

        TestPublishMessage testPublishMessage = new TestPublishMessage();
        testPublishMessage.publishName = customer.customerName;
        testPublishMessage.publishId = Math.toIntExact(customer.id);
        publisher.publish("customer_create", testPublishMessage);

        Bloginfo bloginfo = new Bloginfo();
        bloginfo.name = "getById";
        bloginfo.blogInfo = logger.toString();
        blogInfoCollection.insert(bloginfo);
        return view(customer);
    }
    public void test() {
        executor.submit("test-executor", () -> {
            logger.info("-------------start---------------");
            logger.info(Thread.currentThread().getName());
            Thread.sleep(3000);
            logger.info("-------------end-----------------");
        });
    }

    private CustomerView view(Customer customer) {
        CustomerView customerView = new CustomerView();
        customerView.id = customer.id;
        customerView.customerName = customer.customerName;
        customerView.phone = customer.phone;
        return customerView;
    }

    public void publishTest() {
        for (int i = 0; i < 10; i++) {
            TestPublishMessage testPublishMessage = new TestPublishMessage();
            testPublishMessage.publishName = "allen-" + i;
            testPublishMessage.publishId = i;
            publisher.publish(String.valueOf(testPublishMessage.publishId), testPublishMessage);
        }
    }

    public SearchBlogInfoResponse search(SearchBlogInfoRequest request) {
        SearchBlogInfoResponse searchBlogInfoResponse = new SearchBlogInfoResponse();
        Query query = new Query();
        query.skip = request.skip;
        query.limit = request.limit;
        query.filter = Filters.and(Filters.eq("name", request.name));
        List<Bloginfo> bloginfos = blogInfoCollection.find(query);
        searchBlogInfoResponse.blogInfoViews = bloginfos.stream().map(this::blogInfoView).collect(Collectors.toList());
        searchBlogInfoResponse.total = Math.toIntExact(blogInfoCollection.count(query.filter));
        return searchBlogInfoResponse;
    }

    private BlogInfoView blogInfoView(Bloginfo bloginfo) {
        BlogInfoView blogInfoView = new BlogInfoView();
        blogInfoView.name = bloginfo.name;
        blogInfoView.blogInfo = bloginfo.blogInfo;
        return blogInfoView;
    }
}
