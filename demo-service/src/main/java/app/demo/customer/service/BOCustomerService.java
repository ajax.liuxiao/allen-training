package app.demo.customer.service;

import app.demo.api.customer.BOCustomerView;
import app.demo.api.customer.BOSearchCustomerRequest;
import app.demo.api.customer.BOSearchCustomerResponse;
import app.demo.customer.domain.Bloginfo;
import app.demo.customer.domain.Customer;
import core.framework.db.Database;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.mongo.MongoCollection;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class BOCustomerService {
    private final Logger logger = LoggerFactory.getLogger(BOCustomerService.class);
    @Inject
    Repository<Customer> customerRepository; //TODO use default modifier
    @Inject
    MongoCollection<Bloginfo> blogInfoCollection; //TODO rename TO blogInfoXxxxxxx
    @Inject
    Database database;

    public BOSearchCustomerResponse search(BOSearchCustomerRequest request) {
        BOSearchCustomerResponse searchCustomerResponse = new BOSearchCustomerResponse();
        Query<Customer> query = customerRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.customerName)) {
            query.where("customer_name LIKE ?", Strings.format("{}%", request.customerName));
        }
        if (!Strings.isBlank(request.phone)) {
            query.where("phone = ?", request.phone);
        }
        searchCustomerResponse.customers = query.fetch().stream().map(this::view).collect(Collectors.toList());
        searchCustomerResponse.total = Long.valueOf(query.count());
        return searchCustomerResponse;
    }

    private BOSearchCustomerResponse.Customer view(Customer customer) {
        BOSearchCustomerResponse.Customer customerView = new BOSearchCustomerResponse.Customer();
        customerView.id = customer.id;
        customerView.customerName = customer.customerName;
        customerView.phone = customer.phone;
        return customerView;
    }

    public BOCustomerView searchOne(BOSearchCustomerRequest request) {
        try {
            Customer customer1 = customerRepository.selectOne("customer_name = ?", Strings.format("{}%", request.customerName)).orElseThrow();
            return searchOneView(customer1);
        } catch (NoSuchElementException e) {
            logger.error("SEARCH_ERROR", e);
//            logger.error(Markers.errorCode("SEARCH_ERROR"), e.getMessage());
            throw e;
        }
    }

    private BOCustomerView searchOneView(Customer customer) {
        BOCustomerView boCustomerView = new BOCustomerView();
        boCustomerView.id = customer.id;
        boCustomerView.customerName = customer.customerName;
        boCustomerView.phone = customer.phone;
        return boCustomerView;
    }
}
