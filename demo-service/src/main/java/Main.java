import app.DemoServiceApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoServiceApp().start();
    }
}
