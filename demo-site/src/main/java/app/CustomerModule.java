package app;

import app.demo.CustomerAJAXWebService;
import app.demo.api.CustomerWebService;
import app.demo.customer.web.CustomerAJAXWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class CustomerModule extends Module {
    @Override
    protected void initialize() {
        api().client(CustomerWebService.class, requiredProperty("app.demo.serviceURL")); //TODO rename，
        /**
         * 1. URL is a abbreviation, correct key is xxxURL
         * 2. client key is start with micro service name, such as app.demo.serviceURL
         */
        api().service(CustomerAJAXWebService.class, bind(CustomerAJAXWebServiceImpl.class));
    }
}
