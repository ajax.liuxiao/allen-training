package app.demo.customer.web;

import app.demo.CustomerAJAXWebService;
import app.demo.api.CustomerWebService;
import app.demo.api.customer.CustomerAJAXView;
import app.demo.api.customer.CustomerView;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class CustomerAJAXWebServiceImpl implements CustomerAJAXWebService {
    @Inject
    CustomerWebService customerWebService;

    @Override
    public CustomerAJAXView get(Long id) {

        return view(customerWebService.get(id));
    }

    private CustomerAJAXView view(CustomerView customerView) {
        CustomerAJAXView customerAJAXView = new CustomerAJAXView();
        customerAJAXView.id = customerView.id;
        customerAJAXView.customerName = customerView.customerName;
        customerAJAXView.phone = customerView.phone;
        return customerAJAXView;
    }
}
