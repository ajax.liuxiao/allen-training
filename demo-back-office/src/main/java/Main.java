import app.demo.DemoBackOfficeApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoBackOfficeApp().start();
    }
}
