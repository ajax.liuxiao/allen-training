package app.demo;

import app.demo.api.BOCustomerWebService;
import app.demo.customer.web.CustomerAJAXWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class CustomerModule extends Module {
    @Override
    protected void initialize() {
        api().client(BOCustomerWebService.class, requiredProperty("app.demo.serviceURL"));

        api().service(CustomerAJAXWebService.class, bind(CustomerAJAXWebServiceImpl.class));
    }
}
