package app.demo.customer.web;

import app.demo.CustomerAJAXWebService;
import app.demo.api.BOCustomerWebService;
import app.demo.api.customer.BOSearchCustomerAJAXRequest;
import app.demo.api.customer.BOSearchCustomerAJAXResponse;
import app.demo.api.customer.BOSearchCustomerRequest;
import app.demo.api.customer.BOSearchCustomerResponse;
import core.framework.inject.Inject;

import java.util.ArrayList;

/**
 * @author Allen
 */
public class CustomerAJAXWebServiceImpl implements CustomerAJAXWebService {
    @Inject
    BOCustomerWebService boCustomerWebService;

    @Override
    public BOSearchCustomerAJAXResponse search(BOSearchCustomerAJAXRequest request) {
        BOSearchCustomerRequest searchCustomerRequest = new BOSearchCustomerRequest();
        searchCustomerRequest.skip = request.skip;
        searchCustomerRequest.limit = request.limit;
        searchCustomerRequest.customerName = request.customerName;
        searchCustomerRequest.phone = request.phone;
        BOSearchCustomerResponse search = boCustomerWebService.search(searchCustomerRequest);
        BOSearchCustomerAJAXResponse searchCustomerAJAXResponse = new BOSearchCustomerAJAXResponse();
        searchCustomerAJAXResponse.customers = new ArrayList<>();
        for (BOSearchCustomerResponse.Customer customer : search.customers) {
            BOSearchCustomerAJAXResponse.Customer view = new BOSearchCustomerAJAXResponse.Customer();
            view.id = customer.id;
            view.customerName = customer.customerName;
            view.phone = customer.phone;
            searchCustomerAJAXResponse.customers.add(view);
        }
        searchCustomerAJAXResponse.total = search.total;
        return searchCustomerAJAXResponse;
    }
}
