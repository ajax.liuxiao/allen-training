package app.demo;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Allen
 */
public class DemoBackOfficeApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8083);
        load(new SystemModule("app.properties"));

        load(new CustomerModule());
        load(new OrderModule());
    }
}
