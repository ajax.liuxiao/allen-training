package app.demo;

import app.demo.api.BOOrderWebService;
import app.demo.order.web.OrderAJAXWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class OrderModule extends Module {
    @Override
    protected void initialize() {
        api().client(BOOrderWebService.class, requiredProperty("app.demo.serviceURL"));

        api().service(OrderAJAXWebService.class, bind(OrderAJAXWebServiceImpl.class));
    }
}
