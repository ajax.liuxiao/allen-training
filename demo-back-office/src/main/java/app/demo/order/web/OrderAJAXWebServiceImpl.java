package app.demo.order.web;

import app.demo.OrderAJAXWebService;
import app.demo.api.BOOrderWebService;
import app.demo.api.order.BOSearchOrderSaleAJAXRequest;
import app.demo.api.order.BOSearchOrderSaleAJAXResponse;
import app.demo.api.order.SearchOrderSaleRequest;
import app.demo.api.order.SearchOrderSaleResponse;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class OrderAJAXWebServiceImpl implements OrderAJAXWebService {
    @Inject
    BOOrderWebService boOrderWebService;
    @Override
    public BOSearchOrderSaleAJAXResponse search(BOSearchOrderSaleAJAXRequest request) {
        SearchOrderSaleRequest searchOrderSaleRequest = new SearchOrderSaleRequest();
        searchOrderSaleRequest.month = request.month;
        return view(boOrderWebService.search(searchOrderSaleRequest));
    }

    private BOSearchOrderSaleAJAXResponse view(SearchOrderSaleResponse saleResponse) {
        BOSearchOrderSaleAJAXResponse boSearchOrderSaleAJAXResponse = new BOSearchOrderSaleAJAXResponse();
        boSearchOrderSaleAJAXResponse.totalMoney = saleResponse.totalMoney;
        return boSearchOrderSaleAJAXResponse;
    }
}
