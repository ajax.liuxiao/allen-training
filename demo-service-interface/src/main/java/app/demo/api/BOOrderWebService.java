package app.demo.api;

import app.demo.api.order.SearchOrderSaleRequest;
import app.demo.api.order.SearchOrderSaleResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface BOOrderWebService {
    @GET
    @Path("/order/sale")
    SearchOrderSaleResponse search(SearchOrderSaleRequest request);
}
