package app.demo.api;

import app.demo.api.order.OrderView;
import app.demo.api.order.SearchOrderRequest;
import app.demo.api.order.SearchOrderResponse;
import app.demo.api.order.UpdateOrderStatusRequest;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface OrderWebService {
    @GET
    @Path("/order")
    SearchOrderResponse search(SearchOrderRequest request);

    @PUT
    @Path("/order/status")
    OrderView updateStatus(UpdateOrderStatusRequest request);
}
