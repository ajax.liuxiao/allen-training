package app.demo.api;

import app.demo.api.customer.BOCustomerView;
import app.demo.api.customer.BOSearchCustomerRequest;
import app.demo.api.customer.BOSearchCustomerResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface BOCustomerWebService {
    @GET
    @Path("/bo/customer")
    BOSearchCustomerResponse search(BOSearchCustomerRequest request);

    @GET
    @Path("/bo/customer/selectOne")
    BOCustomerView searchOne(BOSearchCustomerRequest request);

}
