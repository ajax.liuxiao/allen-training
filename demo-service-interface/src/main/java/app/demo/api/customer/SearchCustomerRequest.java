package app.demo.api.customer;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class SearchCustomerRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip = 0;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit = 1000;

    @QueryParam(name = "name") //TODO just use "name" can express you means, because the class name is Search"Customer"Request
    public String customerName;

    @QueryParam(name = "phone")
    public String phone;
}
