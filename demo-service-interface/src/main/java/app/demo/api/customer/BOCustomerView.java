package app.demo.api.customer;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author Allen
 */
public class BOCustomerView {
    @Property(name = "id")
    public Long id;

    @NotNull
    @Property(name = "name")
    public String customerName;

    @Property(name = "phone")
    public String phone;
}
