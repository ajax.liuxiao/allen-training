package app.demo.api.order;

import core.framework.db.Column;

/**
 * @author Allen
 */
public class SearchOrderSaleView {
    @Column(name = "total_money")
    public Double totalMoney;
}
