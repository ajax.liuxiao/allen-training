package app.demo.api.order;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author Allen
 */
public class OrderView {
    @Property(name = "id")
    public String id;

    @NotNull
    @Property(name = "name")
    public String orderName;

    @Property(name = "price")
    public String price;

    @Property(name = "address")
    public String address;

    @Property(name = "customer_id")
    public String customerId;

    @Property(name = "status")
    public String status;

    @Property(name = "updated_time")
    public LocalDateTime updatedTime;
}
