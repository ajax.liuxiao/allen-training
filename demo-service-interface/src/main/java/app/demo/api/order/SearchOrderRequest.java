package app.demo.api.order;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class SearchOrderRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip = 0;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit = 1000;

    @QueryParam(name = "id")
    public String id;

    @QueryParam(name = "name")
    public String orderName;

    @QueryParam(name = "price")
    public Double price;

    @QueryParam(name = "address")
    public String address;

    @QueryParam(name = "customer_id")
    public Long customerId;
}
