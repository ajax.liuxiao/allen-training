package app.demo.api.order;

import core.framework.api.json.Property;

import java.util.List;

/**
 * @author Allen
 */
public class SearchOrderResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "orders")  //TOO haven't follow  "SRP", split to two class
    public List<Order> orders;

    @Property(name = "customers")
    public List<Customer> customers;

    public static class Order {
        @Property(name = "id")
        public String id;

        @Property(name = "name")
        public String orderName;

        @Property(name = "price")
        public String price;

        @Property(name = "address")
        public String address;

        @Property(name = "customer_id")
        public String customerId;

        @Property(name = "status")
        public String status;
    }

    public static class Customer {
        @Property(name = "id")
        public Long id;

        @Property(name = "customer_name")
        public String customerName;

        @Property(name = "phone")
        public String phone;
    }
}
