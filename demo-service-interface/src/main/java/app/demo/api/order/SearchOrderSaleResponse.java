package app.demo.api.order;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class SearchOrderSaleResponse {
    @Property(name = "total_money")
    public Double totalMoney;
}
