package app.demo.api.order;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author Allen
 */
public class UpdateOrderStatusRequest {
    @NotNull
    @Property(name = "id")
    public String id;

    @Property(name = "status")
    public String status;

    @Property(name = "updatedTime")
    public LocalDateTime updatedTime;
}
