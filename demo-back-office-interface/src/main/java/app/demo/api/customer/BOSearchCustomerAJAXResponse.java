package app.demo.api.customer;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author Allen
 */
public class BOSearchCustomerAJAXResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "customers")
    public List<Customer> customers;

    public static class Customer {
        @Property(name = "id")
        public Long id;

        @NotNull
        @Property(name = "customer_name")
        public String customerName;

        @Property(name = "phone")
        public String phone;
    }
}
