package app.demo.api.order;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class BOSearchOrderSaleAJAXRequest {
    @NotNull
    @QueryParam(name = "month")
    public String month;
}
