package app.demo.api.order;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class BOSearchOrderSaleAJAXResponse {
    @Property(name = "total_money")
    public Double totalMoney;
}
