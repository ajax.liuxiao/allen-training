package app.demo.api.order;

import core.framework.db.Column;

/**
 * @author Allen
 */
public class BOSearchOrderSaleAJAXView {
    @Column(name = "total_money")
    public Double totalMoney;
}
