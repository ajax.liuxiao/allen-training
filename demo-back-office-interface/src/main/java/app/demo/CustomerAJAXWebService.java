package app.demo;

import app.demo.api.customer.BOSearchCustomerAJAXRequest;
import app.demo.api.customer.BOSearchCustomerAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface CustomerAJAXWebService {
    @GET
    @Path("/ajax/customer")
    BOSearchCustomerAJAXResponse search(BOSearchCustomerAJAXRequest request);

}
