package app.demo;

import app.demo.api.order.BOSearchOrderSaleAJAXRequest;
import app.demo.api.order.BOSearchOrderSaleAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface OrderAJAXWebService {
    @GET
    @Path("/ajax/order/sale")
    BOSearchOrderSaleAJAXResponse search(BOSearchOrderSaleAJAXRequest request);
}
