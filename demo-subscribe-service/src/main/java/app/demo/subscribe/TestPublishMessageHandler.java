package app.demo.subcribe;

import app.demo.api.customer.kafka.TestPublishMessage;
import core.framework.kafka.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Allen
 */
public class TestPublishMessageHandler implements MessageHandler<TestPublishMessage> {
    private final Logger logger = LoggerFactory.getLogger(TestPublishMessageHandler.class);
    @Override
    public void handle(String key, TestPublishMessage value) throws Exception {
        logger.info("the key is " + key);
        logger.info("the value id is " + value.publishId);
        logger.info("the value name is " + value.publishName);
    }
}
