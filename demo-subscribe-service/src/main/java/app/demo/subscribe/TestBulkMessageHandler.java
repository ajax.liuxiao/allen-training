package app.demo.subcribe;

import app.demo.api.customer.kafka.TestPublishMessage;
import core.framework.kafka.BulkMessageHandler;
import core.framework.kafka.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Allen
 */
public class TestBulkMessageHandler implements BulkMessageHandler<TestPublishMessage> {
    private final Logger logger = LoggerFactory.getLogger(TestBulkMessageHandler.class);
    @Override
    public void handle(List<Message<TestPublishMessage>> messages) throws Exception {
        for (Message<TestPublishMessage> e:messages) {
            logger.info(e.key);
            logger.info(String.valueOf(e.value.publishId));
            logger.info(e.value.publishName);
        }
    }
}
