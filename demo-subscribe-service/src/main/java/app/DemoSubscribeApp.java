package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Allen
 */
public class DemoSubscribeApp extends App { //TODO misspell
    @Override
    protected void initialize() {
        http().httpPort(8888);
        load(new SystemModule("app.properties"));

        load(new SubscribeModule());
    }
}
