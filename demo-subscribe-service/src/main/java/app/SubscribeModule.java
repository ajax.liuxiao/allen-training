package app;

import app.demo.api.customer.kafka.TestPublishMessage;
import app.demo.subcribe.TestBulkMessageHandler;
import app.demo.subcribe.TestPublishMessageHandler;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class SubscribeModule extends Module {
    @Override
    protected void initialize() {
        kafka().uri(requiredProperty("app.kafka.uri"));
        kafka().subscribe("test-topic", TestPublishMessage.class, bind(TestPublishMessageHandler.class));
        kafka().subscribe("test-bulk", TestPublishMessage.class, bind(TestBulkMessageHandler.class));
    }
}
