package app.demo;

import app.demo.api.customer.CustomerAJAXView;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author Allen
 */
public interface CustomerAJAXWebService {
    @GET
    @Path("/ajax/customer/:id")
    CustomerAJAXView get(@PathParam("id") Long id);
}
